package com.apppoweron.newsolution.ui.callback;

/**
 * Created by Papp Balázs on 2015.02.02..
 */
public interface VolleyCallback<T> {
    void onSuccess(T object);
    void onFailure(Exception error);
}

