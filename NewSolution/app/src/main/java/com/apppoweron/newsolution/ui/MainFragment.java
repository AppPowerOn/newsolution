package com.apppoweron.newsolution.ui;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.apppoweron.newsolution.R;


/**
 * A placeholder fragment containing a simple view.
 */
public class MainFragment extends BaseFragment {

    private static final String TAG = "MainFragment";

    public static MainFragment newInstance() {
        return new MainFragment();
    }

    private View mRootView;
    private Button mToFirstButton;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mRootView = inflater.inflate(R.layout.fragment_main, container, false);
        initUI();
        return mRootView;
    }

    private void initUI(){
        mToFirstButton= (Button) mRootView.findViewById(R.id.main_fragment1_btn);
        //mToFirstButton;
    }

    @Override
    protected int getFragmentLogo() {
        return R.mipmap.ic_launcher;
    }

    @Override
    protected String getFragmentTitle() {
        return getString(R.string.main_fragment);
    }
}
