package com.apppoweron.newsolution.ui;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.apppoweron.newsolution.R;

/**
 * Created by GaborPC on 2015.07.03..
 */
public class FourthFragment extends BaseFragment {

    private static final String TAG = "FourthFragment";


    public static FourthFragment newInstance() {
       return new FourthFragment();
    }

    private View mRootView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){
       mRootView = inflater.inflate(R.layout.fragment_fourth, container, false);
       return mRootView;
    }

    @Override
    protected int getFragmentLogo() {
        return R.drawable.Number_4_icon;
    }

    @Override
    protected String getFragmentTitle() {
        return getString(R.string.fragment_4);
    }
}
