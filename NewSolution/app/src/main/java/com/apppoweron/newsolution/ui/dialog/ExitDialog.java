package com.apppoweron.newsolution.ui.dialog;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;


/**
 * Created by G on 2014.11.27..
 */
public class ExitDialog extends DialogFragment {

    private static final String ARG_DIALOG_MESSAGE = "ARG_DIALOG_MESSAGE";

    public static ExitDialog newInstance(String message) {
        ExitDialog exitDialog = new ExitDialog();

        Bundle args = new Bundle();
        args.putString(ARG_DIALOG_MESSAGE, message);
        exitDialog.setArguments(args);

        return exitDialog;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {


        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        //TODO refactor hardcoded part
        builder.setTitle("alert");
        builder.setMessage(getArguments().getString(ARG_DIALOG_MESSAGE));
        //builder.setMessage("placeholder");
        //builder.setIcon(R.drawable.export_icon_achtung);

        builder.setNegativeButton("no", null);

        builder.setPositiveButton("yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                getActivity().finish();
            }
        });

        builder.setCancelable(true);

        // Create the AlertDialog object and return it
        return builder.create();
    }
}
