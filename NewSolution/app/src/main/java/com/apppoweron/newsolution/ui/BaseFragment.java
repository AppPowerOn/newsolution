package com.apppoweron.newsolution.ui;

import android.app.Activity;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.apppoweron.newsolution.ui.callback.FragmentLoaderInterface;


/**
 * Created by Gábor on 2014.11.20..
 */
public abstract class BaseFragment extends Fragment {

    private static final String TAG ="BaseFragment";

    protected FragmentLoaderInterface mCallback;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        // This makes sure that the container activity has implemented
        // the callback interface. If not, it throws an exception
        try {
            mCallback = (FragmentLoaderInterface) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement FragmentLoaderInterface");
        }
    }


    @Override
    public void onStart() {
        super.onStart();
        setFragmentLogo(getFragmentLogo());
        setFragmentTitle(getFragmentTitle());
    }

    protected abstract int getFragmentLogo();

    private void setFragmentLogo(int icon){
        ActionBar actionBar=((AppCompatActivity)getActivity()).getSupportActionBar();
        if(icon!=0) {
            if (actionBar != null) {
                actionBar.setLogo(icon);
            } else {
                Log.e(TAG, "Actionbar is null");
            }
        }else{
            Log.d(TAG, "Icon will be not shown");
        }
    }

    protected abstract String getFragmentTitle();

    private void setFragmentTitle(String title){
        if(title!=null) {
            getActivity().setTitle(title);
        }else{
            Log.d(TAG,"Fragment title is unchanged");
        }
    }


}
