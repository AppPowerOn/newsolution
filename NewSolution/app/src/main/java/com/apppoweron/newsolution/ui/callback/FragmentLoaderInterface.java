package com.apppoweron.newsolution.ui.callback;


import android.support.v4.app.Fragment;

/**
 * Created by Gábor on 2014.11.21..
 */
public interface FragmentLoaderInterface {
    void onNewFragmentSelected(Fragment fragment, boolean repleaceIt, boolean needToBackStack);
    void onNeedToClearBackStack();
}
