package com.apppoweron.newsolution.ui;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.apppoweron.newsolution.R;

/**
 * Created by GaborPC on 2015.07.03..
 */
public class SecondFragment extends BaseFragment{
    
    private static final String TAG = "SecondFragment";
    
    
    public static SecondFragment newInstance() {
       return new SecondFragment();
    }
    
    private View mRootView;
    
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){
       mRootView = inflater.inflate(R.layout.fragment_second, container, false);
    
       return mRootView;
    }

    @Override
    protected int getFragmentLogo() {
        return 0;
    }

    @Override
    protected String getFragmentTitle() {
        return null;
    }
}
