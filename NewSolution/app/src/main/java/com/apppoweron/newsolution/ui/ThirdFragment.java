package com.apppoweron.newsolution.ui;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.apppoweron.newsolution.R;

/**
 * Created by GaborPC on 2015.07.03..
 */
public class ThirdFragment extends BaseFragment {

    private static final String TAG = "ThirdFragment";


    public static ThirdFragment newInstance() {
       return new ThirdFragment();
    }

    private View mRootView;
    private Button mToFourthButton;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){
       mRootView = inflater.inflate(R.layout.fragment_third, container, false);
        initUI();
       return mRootView;
    }

    private void initUI(){
        mToFourthButton= (Button) mRootView.findViewById(R.id.third_to_fourth_btn);
        mToFourthButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCallback.onNewFragmentSelected(FourthFragment.newInstance(),true,true);
            }
        });
    }

    @Override
    protected int getFragmentLogo() {
        return R.drawable.Number_3_icon;
    }

    @Override
    protected String getFragmentTitle() {
        return getString(R.string.fragment_3);
    }
}
