package com.apppoweron.newsolution.ui;


import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import com.apppoweron.newsolution.R;
import com.apppoweron.newsolution.ui.callback.FragmentLoaderInterface;
import com.apppoweron.newsolution.ui.dialog.ExitDialog;


/**
 * Created by Gábor on 2014.11.20..
 */
public abstract class BaseActivity extends AppCompatActivity implements FragmentLoaderInterface {

    private static final String TAG = "BaseActivity";

    private static final String DEFAULT_EXITDIALOG_TAG="DEFAULT_EXITDIALOG_TAG";


    /**
     * Add new fragment to activity
     *
     * @param fragment        fragment which will be loaded
     * @param repleaceIt      want to repleace it or add
     * @param needToBackStack need to backstack current fragment
     */
    protected void addNewFragment(Fragment fragment, boolean repleaceIt, boolean needToBackStack) {
        if (repleaceIt && needToBackStack) {
            getSupportFragmentManager().beginTransaction().replace(R.id.container, fragment).addToBackStack(null)
                    .commitAllowingStateLoss();
        } else if (repleaceIt) {
            getSupportFragmentManager().beginTransaction().replace(R.id.container, fragment)
                    .commitAllowingStateLoss();
        } else if (needToBackStack) {
            getSupportFragmentManager().beginTransaction().add(R.id.container, fragment).addToBackStack(null)
                    .commitAllowingStateLoss();
        } else {
            getSupportFragmentManager().beginTransaction().add(R.id.container, fragment)
                    .commitAllowingStateLoss();
        }

        if (needToBackStack) {
            switchBackButton(true);
        } else {
            switchBackButton(false);
        }

        Log.d(TAG, "Backstack count: " + getFragmentManager().getBackStackEntryCount());
    }

    /**
     * Remove items from backstack
     */
    protected void clearBackStack() {
        getSupportFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
    }

    private void showExitDialog() {
        ExitDialog exitDialog = ExitDialog.newInstance(getExitDialogText());
        exitDialog.show(getSupportFragmentManager(), DEFAULT_EXITDIALOG_TAG);
    }

    protected abstract String getExitDialogText();


    @Override
    public void onBackPressed() {
        if (getSupportFragmentManager().getBackStackEntryCount() > 0) {
            if (getSupportFragmentManager().getBackStackEntryCount() == 1) {
                switchBackButton(false);
            }
            getSupportFragmentManager().popBackStack();
        } else {
            showExitDialog();
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_empty, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }


    /**
     * Show backbutton on activity
     *
     * @param turnOn show it or not
     */
    private void switchBackButton(boolean turnOn) {

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(turnOn);
            getSupportActionBar().setHomeButtonEnabled(turnOn);
        }
    }

    @Override
    public void onNewFragmentSelected(Fragment fragment, boolean repleaceIt, boolean needToBackStack) {
        addNewFragment(fragment,repleaceIt,needToBackStack);
    }

    @Override
    public void onNeedToClearBackStack() {
        clearBackStack();
    }

}
