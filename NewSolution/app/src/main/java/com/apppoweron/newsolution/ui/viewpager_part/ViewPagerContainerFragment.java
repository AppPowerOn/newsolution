package com.apppoweron.newsolution.ui.viewpager_part;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.apppoweron.newsolution.R;
import com.apppoweron.newsolution.ui.BaseFragment;
import com.apppoweron.newsolution.ui.view.MyPagerAdapterFragment;
import com.apppoweron.newsolution.ui.view.SlidingTabLayout;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * Created by GaborPC on 2015.07.03..
 */
public class ViewPagerContainerFragment extends BaseFragment {

    private static final String TAG = "ViewPagerContainerFragment";


    public static ViewPagerContainerFragment newInstance() {
        return new ViewPagerContainerFragment();
    }

    private View mRootView;

    private MyPagerAdapterFragment mMyPagerAdapterFragment;
    private SlidingTabLayout mSlidingTabLayout;
    private ViewPager mViewPager;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mRootView = inflater.inflate(R.layout.fragment_viewpager_container, container, false);
        initUI();
        return mRootView;
    }

    private void initUI() {
        mMyPagerAdapterFragment = new MyPagerAdapterFragment(getChildFragmentManager(), getResources().getStringArray(R.array.view_pager_titles),
                new ArrayList<>(Arrays.asList(PageOneFragment.newInstance(), PageSecondFragment.newInstance())));

        mViewPager = (ViewPager) mRootView.findViewById(R.id.event_detail_container_viewpager);
        mViewPager.setAdapter(mMyPagerAdapterFragment);

        mSlidingTabLayout = (SlidingTabLayout) mRootView.findViewById(R.id.event_detail_container_sliding_tabs);
        mSlidingTabLayout.setViewPager(mViewPager, getResources().getStringArray(R.array.view_pager_titles).length);
    }


    public Fragment getFragmentByPos(int position) {
        return mMyPagerAdapterFragment.getRegisteredFragment(position);
    }

    @Override
    protected int getFragmentLogo() {
        return R.drawable.documents_yellow_icon;
    }

    @Override
    protected String getFragmentTitle() {
        return getString(R.string.viewpager_fragment);
    }
}
