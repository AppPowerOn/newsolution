package com.apppoweron.newsolution.ui.viewpager_part;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.apppoweron.newsolution.R;
import com.apppoweron.newsolution.ui.BaseFragment;

/**
 * Created by GaborPC on 2015.07.03..
 */
public class PageSecondFragment extends BaseFragment {

private static final String TAG = "PageSecondFragment";


public static PageSecondFragment newInstance() {
   return new PageSecondFragment();
}

private View mRootView;

@Override
public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){
   mRootView = inflater.inflate(R.layout.fragment_page_second, container, false);

   return mRootView;
}

    @Override
    protected int getFragmentLogo() {
        return 0;
    }

    @Override
    protected String getFragmentTitle() {
        return null;
    }
}
