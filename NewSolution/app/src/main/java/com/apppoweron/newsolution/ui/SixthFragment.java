package com.apppoweron.newsolution.ui;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.apppoweron.newsolution.R;
import com.apppoweron.newsolution.ui.viewpager_part.ViewPagerContainerFragment;

/**
 * Created by GaborPC on 2015.07.03..
 */
public class SixthFragment extends BaseFragment {

    private static final String TAG = "SixthFragment";


    public static SixthFragment newInstance() {
        return new SixthFragment();
    }

    private View mRootView;
    private Button mToViewPagerButton;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mRootView = inflater.inflate(R.layout.fragment_sixth, container, false);
        initUI();
        return mRootView;
    }

    private void initUI() {
        mToViewPagerButton = (Button) mRootView.findViewById(R.id.sixth_to_pager_btn);
        mToViewPagerButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCallback.onNewFragmentSelected(ViewPagerContainerFragment.newInstance(),true,true);
            }
        });
    }

    @Override
    protected int getFragmentLogo() {
        return R.drawable.Number_6_icon;
    }

    @Override
    protected String getFragmentTitle() {
        return getString(R.string.fragment_6);
    }
}
