package com.apppoweron.newsolution.ui.view;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.os.Build;
import android.view.View;

public class ProgressSwitcher {

    private final View mContentView;
    private final View mProgressView;

    private boolean mHideContent = true;

    private int mHideMethod = View.GONE;

    public ProgressSwitcher(View contentView, View progressView){
        mContentView = contentView;
        mProgressView = progressView;
    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    public void showProgress(final boolean show) {
        // On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
        // for very easy animations. If available, use these APIs to fade-in
        // the progress spinner.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            int shortAnimTime = mContentView.getResources().getInteger(android.R.integer.config_shortAnimTime);

            mContentView.setVisibility(show && mHideContent ? mHideMethod : View.VISIBLE);
            mContentView.animate().setDuration(shortAnimTime).alpha(
                    show && mHideContent ? 0 : 1).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mContentView.setVisibility(show && mHideContent ? mHideMethod : View.VISIBLE);
                }
            });

            mProgressView.setVisibility(show ? View.VISIBLE : mHideMethod);
            mProgressView.animate().setDuration(shortAnimTime).alpha(
                    show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mProgressView.setVisibility(show ? View.VISIBLE : mHideMethod);
                }
            });
        } else {
            // The ViewPropertyAnimator APIs are not available, so simply show
            // and hide the relevant UI components.
            mProgressView.setVisibility(show && mHideContent ? View.VISIBLE : mHideMethod);
            mContentView.setVisibility(show ? mHideMethod : View.VISIBLE);
        }
    }

    public void setHideContent(boolean hideContent){
        mHideContent = hideContent;
    }

    public void setHideMethod(int method){
        mHideMethod = method;
    }
}
