package com.apppoweron.newsolution.ui.view;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.util.SparseArray;
import android.view.ViewGroup;

import java.util.List;

/**
 * Created by G on 2015.02.27..
 */
public class MyPagerAdapterFragment extends FragmentPagerAdapter {

    private String[] mPageTitle;
    private List<Fragment> mFragmentList;
    SparseArray<Fragment> registeredFragments = new SparseArray<>();

    public MyPagerAdapterFragment(FragmentManager fragmentManager, String[] pagerTitle, List<Fragment> fragmentList) {
        super(fragmentManager);
        this.mPageTitle =pagerTitle;
        this.mFragmentList=fragmentList;
    }

    @Override
    public Fragment getItem(int position) {
        // getItem is called to instantiate the fragment for the given page.
        return mFragmentList.get(position);
    }

    @Override
    public int getCount() {
        return mPageTitle.length;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return mPageTitle[position];
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        Fragment fragment = (Fragment) super.instantiateItem(container, position);
        registeredFragments.put(position, fragment);
        return fragment;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        registeredFragments.remove(position);
        super.destroyItem(container, position, object);
    }

    public Fragment getRegisteredFragment(int position) {
        return registeredFragments.get(position);
    }


}
