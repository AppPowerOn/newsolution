package com.apppoweron.newsolution.bo;

/**
 * Created by G on 2015.02.26..
 */
public class MenuOptions {

    public boolean notification=false;
    public boolean search=false;

    public MenuOptions(boolean notification, boolean search) {
        this.notification = notification;
        this.search = search;
    }
}
