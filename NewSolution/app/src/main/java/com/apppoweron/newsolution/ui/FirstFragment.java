package com.apppoweron.newsolution.ui;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.apppoweron.newsolution.R;

/**
 * Created by GaborPC on 2015.07.03..
 */
public class FirstFragment extends BaseFragment {

    private static final String TAG = "FirstFragment";


    public static FirstFragment newInstance() {
       return new FirstFragment();
    }

    private View mRootView;
    private Button mToSecondButton;
    private Button mToThirdButton;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){
       mRootView = inflater.inflate(R.layout.fragment_first, container, false);
        initUI();
       return mRootView;
    }

    private void initUI(){
        mToSecondButton= (Button) mRootView.findViewById(R.id.first_to_second_btn);
        mToSecondButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

        mToThirdButton= (Button) mRootView.findViewById(R.id.first_to_third_btn);
        mToThirdButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
    }

    @Override
    protected int getFragmentLogo() {
        return R.drawable.Number_1_icon;
    }

    @Override
    protected String getFragmentTitle() {
        return getString(R.string.fragment_1);
    }
}
